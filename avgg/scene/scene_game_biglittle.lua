
local money = 5
--0重新开始 1游戏中 2输光
local state = 0
local function draw()
    love.graphics.setColor(255, 255, 0, 255)
    love.graphics.print("压大小王！", 200, 100)
    love.graphics.reset()
    if state==0 then
        love.graphics.print("游戏开始，现在你有"..money.."块钱。", 200, 150)
    elseif state==1 then
        love.graphics.print("你赢啦！，现在你有"..money.."块钱。", 200, 150)
    else
        love.graphics.print("你输光了所有的钱。", 200, 150)
    end
    love.graphics.print("1:小 2:大 3:重新开始", 100, 450)
    love.graphics.print("Press 'Enter' to return.", 100, 500)
end

local function keypressed(key)
    if key == 'return' then
        local s = require('engine/scene')
        s.retreat()
    elseif key=='1' or key=='2' then
        if state==0 or state==1 then
            if math.random(2)==1 then
                state=2
            else
                state=1
                money=money*2
            end
        end
    elseif key=='3' then
        state=0
        money=5
    end
end

local x = {}
x.draw = draw
x.keypressed = keypressed
return x