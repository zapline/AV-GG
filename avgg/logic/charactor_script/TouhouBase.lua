local anim8 = require '3rd/anim8'

local TouhouBase = {}

TouhouBase.xPos = 0

TouhouBase.yPos = 0

TouhouBase.framerate = 0.15

TouhouBase.vximage = nil;

TouhouBase.curAnimation = nil;

TouhouBase.moveAnimation = {}

TouhouBase.stopAnimation = {}

TouhouBase.animationInit = function(self)
    local grid = anim8.newGrid(37, 37, 111, 296)
    self.moveAnimation["up"] = anim8.newAnimation(grid("1-3", "1-1"), self.framerate)
    self.moveAnimation["down"] = anim8.newAnimation(grid("1-3", "5-5"), self.framerate)
    self.moveAnimation["left"] = anim8.newAnimation(grid("1-3", "7-7"), self.framerate)
    self.moveAnimation["right"] = anim8.newAnimation(grid("1-3", "3-3"), self.framerate)

    self.stopAnimation["up"] = anim8.newAnimation(grid("2-2", "1-1"), self.framerate)
    self.stopAnimation["down"] = anim8.newAnimation(grid("2-2", "5-5"), self.framerate)
    self.stopAnimation["left"] = anim8.newAnimation(grid("2-2", "7-7"), self.framerate)
    self.stopAnimation["right"] = anim8.newAnimation(grid("2-2", "3-3"), self.framerate)

    self.curAnimation = self.stopAnimation["down"]
end

TouhouBase.draw = function(self)
    self.curAnimation:draw(self.vximage, self.xPos, self.yPos)
end

TouhouBase.move = function(self, direction)
     self.curAnimation = self.moveAnimation[direction]
end

TouhouBase.moveStop = function(self, direction)
     self.curAnimation = self.stopAnimation[direction]
end

TouhouBase.setPos = function(self, x, y)
    self.xPos = x
    self.yPos = y
end

TouhouBase.update = function(self, dt)
    self.curAnimation:update(dt)
end

return TouhouBase