local anim8 = require '3rd/anim8'

local loader = require "3rd/Advanced-Tiled-Loader/loader"
loader.path = "data/map/"

local map = loader.load("maptest.tmx")

local charactor = require "logic/charactor_script/Reimu"

local world = {}

world.draw = function()
    map:draw()
    charactor:draw()
end

world.init = function()
    charactor:animationInit()
end

local curDirection = "down"

local spriteX = 0
local spriteY = 0

local function crashtest(x, y)
    if x < 0 or y < 0 or x > 800-37 or y > 600-37 then
       return false
    end
    return true
end

world.update = function(dt)
    if love.keyboard.isDown("up") then
        curDirection = "up"
        charactor:move(curDirection)
        if crashtest(spriteX, spriteY -1) then
            spriteY = spriteY - 1
            charactor:setPos(spriteX, spriteY)
        end

    elseif love.keyboard.isDown("down") then
        curDirection = "down"
        charactor:move(curDirection)
        if crashtest(spriteX, spriteY + 1) then
            spriteY = spriteY + 1
            charactor:setPos(spriteX, spriteY)
        end

    elseif love.keyboard.isDown("left") then
        curDirection = "left"
        charactor:move(curDirection)
        if crashtest(spriteX - 1, spriteY) then
            spriteX = spriteX - 1
            charactor:setPos(spriteX, spriteY)
        end

    elseif love.keyboard.isDown("right") then
        curDirection = "right"
        charactor:move(curDirection)
        if crashtest(spriteX + 1, spriteY) then
            spriteX = spriteX + 1
            charactor:setPos(spriteX, spriteY)
        end

    else
        charactor:moveStop(curDirection)
    end
    charactor:update(dt)
end

world.keypressed = function(key)
    if key == 'return' then
        local s = require('engine/scene')
        s.switch('main_menu')
    end
end

world.keyrealeased = function(key)
end

return world